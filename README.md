# Halo 2021

Code base for the artwork Halo (2021).

# Dependencies
- python3
- pyglet
- scipy
- pigpio
- gpiozero
- smbus2
- mpu6050-raspberrypi

# Pipenv
### Install
```bash
$ pipenv install
```
### Add packages
```bash
$ pipenv install <package>
```
This will add the requirement to the pipfile.

### Update dependencies
Check what's available:
```bash
pipenv update --outdated
```
Update all:
```bash
pipenv update
```

Update specific:
```bash
pipenv update <pkg>
```

### Enter virtual environment
To run the environment python from the command line:
```bash
$ pipenv shell
$ which python
```
### Exit pipenv

```bash
$ exit
```
### Help
- [pipenv docs](https://pipenv-fork.readthedocs.io/en/latest/basics.html)
- [pipenv guide](https://realpython.com/pipenv-guide/)

# RPi
### Install raspian
- Download latest image: [Raspbian](https://www.raspberrypi.org/downloads/raspbian/)
- Insert MicroSD card into Mac
- Terminal: `diskutil list`
- Check SD name (e.g. `disk2`) from the list
- Unmount the disk: `diskutil unmountDisk /dev/disk2`
- Copy image `sudo dd bs=1m if=path_of_your_image.img of=/dev/rdisk2; sync`
- Eject image `sudo diskutil eject /dev/rdisk2`
- Connect card to mac
- Add the file in halo/pi/wpa_supplicant.conf to pi/boot/
- Add an empty file called 'ssh' to enable ssh `touch /Volumes/boot/ssh`
- Insert card to Pi and start

Follow [this](https://desertbot.io/blog/headless-raspberry-pi-3-bplus-ssh-wifi-setup)

### Network setup
- `ssh pi@raspberrypi.local`
- Login with default password `raspberry`
- Change password with `passwd`
- Change hostname via `sudo raspi-config` Network Options > Hostname to `halo-unit-<id>`
- Enable `Remote GPIO` with `sudo raspi-config` > Interfaces > Remote GPIO
- Enable i2c Interfacing Options > i2c

### Setup pigpio
Follow [instructions](https://gpiozero.readthedocs.io/en/stable/remote_gpio.html)
- Start Pi and login via ssh
- `sudo apt install pigpio`
- Run pigpio at boot `sudo systemctl enable pigpiod`
- Restart `sudo reboot`

# Run
```bash
cd 2021-halo/halo
python3 .
```

# Testing
## Unit tests
Run `python3 -m unittest` in repository directory for running all unit test files
starting with `test*`.

## Manual tests
Run `python3 -m halo.test.test_manual` from repository directory to run specific file.

## In PyCharm
Set up test configuration (Configuration > Python tests):
- Target "Module Name": `test`

## Copyright
- Artwork ©2020 Tobias Zehntner
- Source code published under License AGPL-3.0 or later
