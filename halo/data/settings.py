SETTINGS = {
    "fps": 30,
    "setup": {
        "generic": {
            "num_rows": 3,
            "num_columns": 3,
            "distance": {"x": 4, "y": 4},  # In meters between units
            "length": 14,  # cable length in meters
            "max_height_diff": 0.5,
            "remote_disable": True,
        },
        "bright2021": {
            "num_rows": 4,
            "num_columns": 2,
            "distance": {"x": 4.5, "y": 6.5},  # In meters between units
            "length": 14,  # max cable length
            "max_height_diff": 0.4,
            "remote_disable": False,
            "i2c": {"bus": 1, "arduino_address": 0x04, "accel_address": 0x68},
            # "dimmer": {"min": 105, "max": 40},
        },
        "orbit2015": {
            "num_rows": 1,
            "num_columns": 2,
            "distance": {"x": 3, "y": 3},  # In meters between units
            "length": 3,  # cable length in meters
            "max_height_diff": 0.05,
            "remote_disable": True,
        },
    },
    "logging": {"format": "%(asctime)s - %(levelname)s - %(message)s"},
    "ui": {
        "window": {"width": 700, "height": 1000},
        "m_to_px_ratio": 30,
        "draw_measures": {
            "bulb_radius": 0.08,
            "bulb_light_radius": 0.1,
            "throw_light_radius": 4.0,
        },
    },
}
