# © 2020 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

########################################################################################
# HALO - Config Data
# Data prepared for import and use
########################################################################################

import logging

from halo.data.settings import SETTINGS
from halo.helpers import helper


def m_to_px(m):
    ratio = SETTINGS["ui"]["m_to_px_ratio"]
    return m * ratio


def get_matrix_dict(rows, columns):
    positions = {}
    unit_id = 0
    for i in range(1, rows + 1):
        for j in range(1, columns + 1):
            unit_id += 1
            positions[unit_id] = (i, j)
    return positions


def get_matrix_pos(rows, columns):
    positions = []
    for i in range(0, rows):
        positions.append([])
        for j in range(0, columns):
            positions[i].append([])
            positions[i][j] = (i, j)
    return positions


def get_setup(name):
    if not name:
        setup = SETTINGS["setup"]["generic"]
        logging.info("Configure with generic setup.")
    else:
        setup = SETTINGS["setup"].get(name)
        if setup:
            logging.info(f"Configure with setup: {name}")
        else:
            setup = SETTINGS["setup"]["generic"]
            logging.warning(f"Setup '{name}' not found. Configure with generic setup.")

    num_rows = setup["num_rows"]
    num_columns = setup["num_columns"]
    num_units = num_rows * num_columns
    matrix_dict = get_matrix_dict(num_rows, num_columns)
    distance = setup["distance"]
    unit_pos_m = {}
    for unit_id, pos in matrix_dict.items():
        unit_pos_m[unit_id] = (
            (pos[1] - 1) * distance["x"],
            (pos[0] - 1) * distance["y"],
        )
    unit_pos_px = {}
    for unit_id, pos in unit_pos_m.items():
        unit_pos_px[unit_id] = (m_to_px(pos[0]), m_to_px(pos[1]))

    max_length = setup["length"]
    max_height_diff = setup["max_height_diff"]
    height_diff = max_height_diff / num_units
    height_diff_ratio = helper.matrix_distribute(num_rows, num_columns)

    units_vals = []
    for unit_id in range(1, num_units + 1):
        unit_pos = matrix_dict[unit_id]
        ratio = height_diff_ratio[unit_pos[0] - 1][unit_pos[1] - 1]
        unit_length = max_length - (height_diff * ratio)
        angle = 10  # TODO get from accellerometer
        start_rotation = ratio * 30
        remote_disable = setup.get("remote_disable", True)
        unit_val = {
            "key": f"halo_unit_{unit_id}",
            "name": f"Halo Unit #{unit_id}",
            "hostname": f"halo-unit-{unit_id}",
            "matrix_pos": unit_pos,
            "matrix_neighbor_pos": helper.get_matrix_neighbors(
                get_matrix_pos(num_rows, num_columns), unit_pos[0] - 1, unit_pos[1] - 1
            ),
            "pos": {"x": unit_pos_px[unit_id][0], "y": unit_pos_px[unit_id][1]},
            "cable_length": unit_length,
            "remote_disable": remote_disable,
            "angle": angle,
            "rpm": helper.get_rpm(unit_length, angle),
            "rotation": start_rotation,
        }
        if not remote_disable:
            unit_val.update(
                {
                    "arduino_address": setup["i2c"]["arduino_address"],
                    "accel_address": setup["i2c"]["accel_address"],
                }
            )
        units_vals.append(unit_val)

    # Draw in center of screen
    ui_config = SETTINGS["ui"]
    dist_x = m_to_px(setup["distance"]["x"])
    dist_y = m_to_px(setup["distance"]["y"])

    draw_pos_x = (ui_config["window"]["width"] / 2) - (((num_columns - 1) * dist_x) / 2)
    draw_pos_y = (ui_config["window"]["height"] / 2) - (((num_rows - 1) * dist_y) / 2)

    ui_vals = {
        "key": "halo_ui",
        "name": "Halo User Interface",
        "width": ui_config["window"]["width"],
        "height": ui_config["window"]["height"],
        "pos": {"x": draw_pos_x, "y": draw_pos_y},
    }

    endpoint_matrix = []
    for i in range(0, num_rows):
        endpoint_matrix.append([])
        for _j in range(0, num_columns):
            endpoint_matrix[i].append([])

    controller_vals = {
        "key": "halo_controller",
        "name": "Halo Controller",
        "dt": 1 / SETTINGS["fps"],
        "endpoint_matrix": endpoint_matrix,
        "min_distance": 0.5,
        "max_distance": (dist_x * 2),  # Max distance between two bulbs (zero dim)
    }
    return controller_vals, units_vals, ui_vals
