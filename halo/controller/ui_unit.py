import itertools

import pyglet

from halo.data.data_loader import m_to_px
from halo.data.settings import SETTINGS
from halo.helpers import helper

from . import base, ui_element

DRAW_MEASURES = SETTINGS["ui"]["draw_measures"]


class UiUnit(base.Base):
    __slots__ = ["unit", "ui_elements", "gl_groups", "ui_element_groups"]
    _id_seq = itertools.count(start=1)

    def __init__(self, **kwargs):
        self.unit = False
        self.ui_elements = {}
        self.gl_groups = {}
        self.ui_element_groups = {}
        super().__init__(**kwargs)
        self.id = next(self._id_seq)

    def draw(self):
        pass


class HaloUiUnit(UiUnit):
    def draw(self):
        super().draw()
        bulb_light_radius = (
            m_to_px(DRAW_MEASURES["bulb_light_radius"]) / 100 * self.unit.brightness
        )
        throw_light_radius = (
            m_to_px(DRAW_MEASURES["throw_light_radius"]) / 100 * self.unit.brightness
        )
        self.ui_elements["unit"].parts[
            "bulb_light"
        ].vertices = helper.get_circle_vertices(
            self.unit.pos["x"] + self.unit.radius, self.unit.pos["y"], bulb_light_radius
        )
        self.ui_elements["unit"].parts[
            "throw_light"
        ].vertices = helper.get_circle_vertices(
            self.unit.pos["x"] + self.unit.radius,
            self.unit.pos["y"],
            throw_light_radius,
        )

        for element in self.ui_elements.values():
            if element.ui_group.is_visible:
                pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
                pyglet.gl.glBlendFunc(
                    pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA
                )
                pyglet.gl.glPushMatrix()
                if element.ui_group.key == "units":
                    # Rotate unit elements
                    pyglet.gl.glTranslatef(self.unit.pos["x"], self.unit.pos["y"], 0.0)
                    pyglet.gl.glRotatef(-self.unit.rotation, 0, 0, 1)
                    pyglet.gl.glTranslatef(
                        -self.unit.pos["x"], -self.unit.pos["y"], 0.0
                    )
                element.batch.draw()
                pyglet.gl.glPopMatrix()

    def set_ui(self, gl_groups, ui_element_groups):
        unit = self.set_draw_unit(gl_groups, ui_element_groups["units"])
        interface = self.set_draw_unit_interface(
            gl_groups, ui_element_groups["interface"]
        )
        self.ui_elements.update({"unit": unit, "interface": interface})

    def set_draw_unit_interface(self, gl_groups, ui_group):
        unit_labels = ui_element.UiElement(
            **{"name": "Unit Labels", "key": "unit_label"}
        )

        label1 = pyglet.text.Label(
            self.unit.name,
            font_name="Helvetica",
            font_size=12,
            x=self.unit.pos["x"],
            y=self.unit.pos["y"] + 100,
            anchor_x="center",
            anchor_y="center",
            color=(255, 255, 255, 255),
            batch=unit_labels.batch,
            group=gl_groups["interface"],
        )
        unit_labels.parts = {"unit_label": label1}
        unit_labels.ui_group = ui_group
        return unit_labels

    def set_draw_unit(self, gl_groups, ui_group):
        unit_element = ui_element.UiElement(**{"name": "Unit", "key": "unit"})

        cable = unit_element.batch.add(
            2,
            pyglet.gl.GL_LINES,
            gl_groups["foreground"],
            (
                "v2f/static",
                (
                    self.unit.pos["x"],
                    self.unit.pos["y"],
                    self.unit.endpoint[0],
                    self.unit.endpoint[1],
                ),
            ),
            ("c4B/static", (0, 0, 0, 255) * 2),  # FIXME set to black
        )

        # Draw bulb as hollow circle
        bulb_radius = m_to_px(DRAW_MEASURES["bulb_radius"])
        bulb_vertices = helper.get_circle_vertices(
            self.unit.endpoint[0], self.unit.endpoint[1], bulb_radius, fill=False
        )
        hollow_circle_index = len(bulb_vertices) // 2
        bulb = unit_element.batch.add(
            hollow_circle_index,
            pyglet.gl.GL_LINE_LOOP,
            gl_groups["middleground"],
            ("v2f/static", bulb_vertices),
            ("c4B/static", (120, 120, 120, 76) * hollow_circle_index),
        )

        # Draw light as filled circle with modifiable vertices
        bulb_light_radius = m_to_px(DRAW_MEASURES["bulb_light_radius"])
        bulb_light_vertices = helper.get_circle_vertices(
            self.unit.endpoint[0], self.unit.endpoint[1], bulb_light_radius
        )
        # Add midpoint to fill circle
        filled_circle_index = hollow_circle_index + 1
        # Gradient: Inside color - outside color
        bulb_light_colors = (249, 242, 204, 255) + (
            (249, 242, 204, 0) * hollow_circle_index
        )
        bulb_light = unit_element.batch.add(
            filled_circle_index,
            pyglet.gl.GL_TRIANGLE_FAN,
            gl_groups["middleground"],
            ("v2f/stream", bulb_light_vertices),
            ("c4B/static", bulb_light_colors),
        )

        # Throw light
        throw_light_radius = m_to_px(DRAW_MEASURES["throw_light_radius"])
        throw_light_vertices = helper.get_circle_vertices(
            self.unit.endpoint[0], self.unit.endpoint[1], throw_light_radius
        )
        throw_light_colors = (213, 148, 67, 80) + ((0, 0, 0, 0) * hollow_circle_index)

        throw_light = unit_element.batch.add(
            filled_circle_index,
            pyglet.gl.GL_TRIANGLE_FAN,
            gl_groups["background"],
            ("v2f/stream", throw_light_vertices),
            ("c4B/static", throw_light_colors),
        )

        unit_element.parts = {
            "cable": cable,
            "bulb": bulb,
            "bulb_light": bulb_light,
            "throw_light": throw_light,
        }
        unit_element.ui_group = ui_group
        return unit_element
