import itertools
import logging

from halo.controller import base


class I2C(base.Base):
    __slots__ = ["pi", "bus", "address", "handler"]
    _id_seq = itertools.count(start=1)

    def __init__(self, bus=1, **kwargs):
        super().__init__(**kwargs)
        self.id = next(self._id_seq)
        self.handler = self.pi.i2c_open(bus, self.address)
        self.check_connection()

    def check_connection(self):
        if not self.handler >= 0:
            raise Exception("Error connecting via i2c")
        self.write_byte(128)
        if self.read_byte() != 128:
            raise Exception(f"Unexpected return from {self.name}")
        logging.info(f"{self.name} connected")

    def read_byte(self):
        return self.pi.i2c_read_byte(self.handler)

    def write_byte(self, byte):
        self.pi.i2c_write_byte(self.handler, byte)

    def disconnect(self):
        self.pi.i2c_close(self.handler)
        logging.info(f"Closed connection to {self.name}")


class Arduino(I2C):
    def disconnect(self):
        # TODO move to dimmer class
        self.write_byte(128)  # Turn off dimmer
        super().disconnect()
