import itertools

import pyglet

from . import base


class UiElement(base.Base):
    __slots__ = ["batch", "parts", "ui_group"]
    _id_seq = itertools.count(start=1)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.id = next(self._id_seq)
        self.batch = pyglet.graphics.Batch()
        self.parts = []
        self.ui_group = False


class UiElementGroup(base.Base):
    __slots__ = ["is_visible"]
    _id_seq = itertools.count(start=1)

    def __init__(self, **kwargs):
        self.is_visible = True  # default can be overwritten in init values
        super().__init__(**kwargs)
        self.id = next(self._id_seq)

    def toggle_visibility(self, visible=None):
        if visible is None:
            self.is_visible = not self.is_visible
        else:
            self.is_visible = visible
