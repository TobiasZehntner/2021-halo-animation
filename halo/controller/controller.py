import itertools
import logging
import time

import pyglet

from halo.helpers import helper

from . import base, ui_unit as halo_ui_unit, unit as halo_unit, user_interface


class Controller(base.Base):
    __slots__ = ["dt", "ui", "units"]
    _id_seq = itertools.count(start=1)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.id = next(self._id_seq)
        self.ui = False
        self.units = []

    def set_ui(self, ui_config):
        pass

    def set_units(self, units_vals):
        pass

    def connect_units(self):
        for unit in self.units:
            unit.connect()

    def disconnect_units(self):
        for unit in self.units:
            unit.disconnect()

    def setup(self):
        pass

    def update(self, dt):
        pass

    def update_ui(self, dt):
        self.update(dt)
        self.ui.draw()

    def run(self, headless=False):
        """
        Set up main loop
        """
        try:
            if not self.dt:
                raise Exception("Missing setting for Frames per Second")

            self.connect_units()

            logging.info(f"{self.name}: Run")
            if headless:

                counter = 0
                start_time = time.time()
                dt = self.dt

                while True:
                    self.update(dt)

                    # Aim for precise interval
                    counter += 1
                    sleep = start_time + (dt * counter) - time.time()
                    if sleep > 0:
                        time.sleep(sleep)
                    else:
                        logging.warning(
                            "Too slow: loop took longer than DT. Reduce f/s."
                        )
            else:
                # TODO do f/s check
                pyglet.clock.schedule_interval(self.update_ui, self.dt)
                pyglet.app.run()

        except KeyboardInterrupt:
            logging.info("Exiting...")
        finally:
            logging.info("Shutting down...")
            self.disconnect_units()
            logging.info("Done.")


class HaloController(Controller):
    __slots__ = ["endpoint_matrix", "min_distance", "max_distance"]

    def set_ui(self, ui_config):
        super().set_ui(ui_config)
        self.ui = user_interface.HaloUserInterface(**ui_config)
        gl_groups = self.ui.gl_groups
        ui_element_groups = self.ui.ui_element_groups
        for unit in self.units:
            ui_unit = halo_ui_unit.HaloUiUnit(
                **{
                    "unit": unit,
                    "gl_groups": gl_groups,
                    "ui_element_groups": ui_element_groups,
                }
            )
            ui_unit.set_ui(gl_groups, ui_element_groups)
            self.ui.ui_units.append(ui_unit)

    def set_units(self, units_vals):
        super().set_units(units_vals)
        units = []
        for vals in units_vals:
            units.append(halo_unit.HaloUnit(**vals))
        self.units = units

    def update(self, dt):
        """
        1. Update position
        2. Compute brightness
        3. Send data
        :param dt:
        :type dt:
        """
        super().update(dt)
        self.update_positions(dt)
        self.compute_brightness()

    def update_positions(self, dt):
        for unit in self.units:
            unit.update_rotation(dt)
            self.endpoint_matrix[unit.matrix_pos[0] - 1][
                unit.matrix_pos[1] - 1
            ] = unit.endpoint

    def compute_brightness(self):
        for unit in self.units:
            distances = []
            for neighbor in unit.matrix_neighbor_pos:
                distance = helper.get_distance_between_points(
                    unit.endpoint, self.endpoint_matrix[neighbor[0]][neighbor[1]]
                )
                distances.append(distance)
            unit.brightness = helper.map_range(
                min(distances), self.min_distance, self.max_distance, 100, 0
            )
