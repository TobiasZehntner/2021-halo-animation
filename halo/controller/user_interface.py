import itertools

import pyglet
from pyglet.window import key

from . import base, ui_element


class UIWindow(pyglet.window.Window):
    __slots__ = ["ui"]

    def on_key_press(self, symbol, modifiers):
        if symbol == key.SPACE:
            # Toggle Interface on <space> press
            self.ui.ui_element_groups["interface"].toggle_visibility()


class UserInterface(base.Base):
    __slots__ = [
        "window",
        "fps_display",
        "width",
        "height",
        "gl_groups",
        "pos",
        "ui_element_groups",
        "ui_units",
    ]
    _id_seq = itertools.count(start=1)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.id = next(self._id_seq)
        self.window = UIWindow()
        self.fps_display = pyglet.window.FPSDisplay(window=self.window)
        self.window.ui = self
        self.window.width = self.width
        self.window.height = self.height

        self.gl_groups = {
            "background": pyglet.graphics.OrderedGroup(0),
            "middleground": pyglet.graphics.OrderedGroup(1),
            "foreground": pyglet.graphics.OrderedGroup(2),
            "interface": pyglet.graphics.OrderedGroup(3),
        }
        self.ui_element_groups = {}
        self.ui_units = []

    def draw(self):
        pyglet.gl.glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
        pyglet.gl.glLoadIdentity()
        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslatef(self.pos["x"], self.pos["y"], 0.0)

        if self.ui_element_groups["interface"].is_visible:
            self.fps_display.draw()

        for ui_unit in self.ui_units:
            ui_unit.draw()

        pyglet.gl.glPopMatrix()


class HaloUserInterface(UserInterface):
    __slots__ = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        unit_group = ui_element.UiElementGroup(**{"key": "units", "name": "Units"})
        interface_group = ui_element.UiElementGroup(
            **{"key": "interface", "name": "Interface"}
        )
        self.ui_element_groups = {"units": unit_group, "interface": interface_group}
