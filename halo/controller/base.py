class Base:
    __slots__ = ["id", "key", "name"]

    def __init__(self, **kwargs):
        """
        When class is initiated with Base({key: value, key2: value2}) the attributes
        defined in __slots__ are assigned to the object.
        @param kwargs: key,value dict with attributes for the initiated object
        @type kwargs: dict
        """
        for key, value in kwargs.items():
            setattr(self, key, value)
