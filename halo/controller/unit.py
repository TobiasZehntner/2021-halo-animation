import itertools
import logging
import math

import pigpio

from halo.controller import base, i2c
from halo.data.data_loader import m_to_px
from halo.helpers import helper


class Unit(base.Base):
    __slots__ = ["hostname", "pi", "remote_disable"]
    _id_seq = itertools.count(start=1)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.id = next(self._id_seq)
        self.pi = False

    def connect(self):
        self.connect_to_pi()

    def connect_to_pi(self):
        self.pi = False
        if self.remote_disable:
            return
        if not self.hostname:
            raise BaseException(f"Missing hostname for {self.name}")

        host = f"{self.hostname}.local"
        if helper.ping(host):
            self.pi = pigpio.pi(host)
            if self.pi.connected:
                logging.info(f"Connected to remote {self.name}")
            else:
                self.pi = False
                logging.warning(f"Connection to remote {self.name} ({host}) failed.")

        else:
            logging.warning(
                f"Could not find {self.name} on local network ({host}). "
                f"If this is intended, run with '--remote-disable {self.id}'"
            )

    def disconnect(self):
        if self.pi:
            self.pi.stop()
            logging.info(f"Closed connection to Pi {self.name}")


class HaloUnit(Unit):
    __slots__ = [
        "matrix_pos",
        "matrix_neighbor_pos",
        "pos",
        "cable_length",
        "endpoint",
        "radius",
        "arduino",
        "arduino_address",
        "accel_address",
        "rpm",
        "rotation",
        "angle",
        "brightness",
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.radius = m_to_px(2)  # FIXME replace with angle calc
        self.endpoint = self.get_endpoint(0)
        self.arduino = None
        self.brightness = 0

    def connect(self):
        super().connect()
        self.connect_to_arduino()

    def connect_to_arduino(self):
        if self.pi:
            try:
                vals = {
                    "name": f"Arduino {self.name}",
                    "pi": self.pi,
                    "address": self.arduino_address,
                }
                self.arduino = i2c.Arduino(**vals)
            except Exception as error:
                logging.error(f"{self.name} failed to connect to Arduino: {error}")

    def disconnect(self):
        if self.pi and self.arduino:
            self.arduino.disconnect()
        super().disconnect()

    def get_endpoint(self, rotation):
        rotation_radians = -math.radians(rotation)
        return (
            math.cos(rotation_radians) * self.radius + self.pos["x"],
            math.sin(rotation_radians) * self.radius + self.pos["y"],
        )

    def update_rotation(self, dt):
        """
        deg/s = rpm * 6
        deg = deg/s * dt
        """
        self.rotation += self.rpm * 6 * dt
        self.endpoint = self.get_endpoint(self.rotation)
