import argparse
import logging
import sys

from controller import controller
from data.data_loader import get_setup
from data.settings import SETTINGS

logging.basicConfig(level=logging.INFO, format=SETTINGS["logging"]["format"])


class ArgParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write("error: %s\n" % message)
        self.print_help(sys.stderr)
        sys.exit(2)


parser = ArgParser(description="Run Halo controller")
parser.add_argument("--debug", help="debug mode", action="store_true")
parser.add_argument("--headless", action="store_true", help="disable user interface")
parser.add_argument("-s", "--setup", help="use specific instead of generic setup")

# Argument groups
remote_group = parser.add_mutually_exclusive_group()
remote_group.add_argument(
    "--remote-disable",
    nargs="*",
    help="disable remote [<id> <id> ...]. All if not specified.",
)
remote_group.add_argument(
    "--remote-enable",
    nargs="*",
    help="enable remote [<id> <id> ...]. All if not specified.",
)

args = parser.parse_args()

if args.headless:
    logging.info("Running Halo controller without user interface")
if args.debug:
    logging.info("Running Halo in Debug Mode")


if __name__ == "__main__":
    controller_vals, units_vals, ui_vals = get_setup(args.setup)

    if args.remote_disable is not None or args.remote_enable is not None:
        # Treat args for remote:
        # "--remote_disable" -> disable all remotes
        # "--remote_disable 4 5" -> disable remotes for unit 4 and 5
        # #--remote_enable" -> enable all remotes
        # "--remote_enable 1 3" -> enable remotes for 1 and 3
        units = (
            args.remote_disable
            if args.remote_disable is not None
            else args.remote_enable
        )
        disable = args.remote_disable is not None or args.remote_enable is None
        all_units = units == []
        units = (args.remote_disable or args.remote_enable) if not all_units else []
        unit_ids = [int(x) for x in units]
        for i, vals in enumerate(units_vals):
            if all_units or (i + 1) in unit_ids:
                vals["remote_disable"] = disable
            else:
                vals["remote_disable"] = not disable

        logging.info(
            f"Remote {disable and 'disabled' or 'enabled'}"
            f"{': ' + str(unit_ids) if not all_units else ''}"
        )

    controller = controller.HaloController(**controller_vals)
    controller.set_units(units_vals)
    if not args.headless:
        controller.set_ui(ui_vals)
    controller.run(headless=args.headless)
