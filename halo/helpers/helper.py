import math
import os
import platform
import subprocess

from scipy.constants import g as GRAVITY


def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    """
    param = "-n" if platform.system().lower() == "windows" else "-c"
    command = ["ping", param, "1", host]

    with open(os.devnull, "w") as DEVNULL:
        try:
            subprocess.check_call(
                command, stdout=DEVNULL, stderr=DEVNULL  # suppress output
            )
            return True
        except subprocess.CalledProcessError:
            return False


def get_rpm(cable_length, angle):
    """
    Get rpm speed of a cononical pendulum
    @param angle: angle of cable
    @return: rpm
    :param cable_length: cable length in meters
    :type cable_length: float
    """
    return (
        (math.sqrt(GRAVITY / (cable_length * math.cos(math.radians(angle)))))
        / (2 * math.pi)
    ) * 60


def matrix_distribute(num_rows, num_columns):
    """
    Returns a matrix with cell numbers having a maximum difference to their direct
    neighbors.

    Sample matrix:
    A B C D E F G
    H I J K L M N
    O P Q R S T U
    iterates diagonally, with two counters for every even/odd line:
    A           1
    H B         12, 13,
    O I C       2, 3, 4,
    P J D       14, 15, 16,
    Q K E       5, 6, 7,
    R L F       17, 18, 19,
    S M G       8, 9, 10,
    T N         20, 21,
    U           11
    returns:
    [
        [1, 13, 4, 16, 7, 19, 10],
        [12, 3, 15, 6, 18, 9, 21],
        [2, 14, 5, 17, 8, 20, 11],
    ]

    Since the distribution is more ideal in landscape format, we turn any portrait
    matrix before and after the distribution.
    @param num_rows: Number of rows
    @type num_rows: int
    @param num_columns: Number of columns
    @type num_columns: int
    @return: List of lists
    @rtype: list
    """
    rotate = False
    if num_columns < num_rows:
        old_rows = num_rows
        num_rows = min(num_rows, num_columns)
        num_columns = max(old_rows, num_columns)
        rotate = True

    matrix = []
    for x in range(num_rows):
        matrix.append([])
        for _y in range(num_columns):
            matrix[x].append([])

    counter_odd = 1
    counter_even = math.ceil(((num_rows * num_columns) / 2) + 1)
    even = False
    for k in range(num_rows + num_columns - 2 + 1):
        for j in range(k + 1):
            i = k - j
            if i < num_rows and j < num_columns:
                if even:
                    matrix[i][j] = counter_even
                    counter_even += 1
                else:
                    matrix[i][j] = counter_odd
                    counter_odd += 1
        even = not even

    if rotate:
        rotated = list(zip(*matrix[::-1]))
        matrix = [list(elem) for elem in rotated]

    return matrix


def get_matrix_neighbors(matrix, row_num, col_num):
    """
    For a given row/column position in a matrix, return [(row, column)] of immediate
    neighbors (horizontal, vertical, diagonal)

    matrix = [
        [a, b, c],
        [d, e, f],
        [g, h, i],
    ]
    for e, return [a, b, c, d, f, g, h, i]
    for g, return [d, e, h]

    :param matrix:
    :type matrix: list of lists
    :param row_num: row number
    :type row_num: int
    :param col_num: column number
    :type col_num: int
    :return: [(neighbor1_row, neighbor1_column)]
    :rtype: list
    """
    neighbor_positions = []
    for i, row in enumerate(matrix):
        if row_num - 1 <= i <= row_num + 1:
            for j, _col in enumerate(row):
                if col_num - 1 <= j <= col_num + 1:
                    if i == row_num and j == col_num:
                        continue
                    else:
                        neighbor_positions.append((i, j))
    return neighbor_positions


def get_distance_between_points(x1y1, x2y2):
    """
    Return distance between two points
    :param x1y1: (x_pos1, y_pos1)
    :type x1y1: tuple
    :param x2y2: (x_pos2, y_pos2)
    :type x2y2: tuple
    :return:
    :rtype: float
    """
    a = abs(x1y1[0] - x2y2[0])
    b = abs(x1y1[1] - x2y2[1])
    return math.sqrt((a ** 2) + (b ** 2))


def map_range(x, in_min, in_max, out_min, out_max):
    """
    Arduino's map() function: Re-maps a number from one range to another
    """
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def get_circle_vertices(x, y, radius, fill=True):
    pts = 360
    s = math.sin(2 * math.pi / pts)
    c = math.cos(2 * math.pi / pts)

    vertices = []
    if fill:
        vertices += [x, y]
    dx, dy = radius, 0
    for _i in range(pts + 1):
        vertices += [x + dx, y + dy]
        dx, dy = (dx * c - dy * s), (dy * c + dx * s)

    return vertices
