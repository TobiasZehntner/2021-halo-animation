// -*- coding: utf-8 -*-
// © 2021 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

/////////////////////////////////////////////////////
// i2c test: receives byte and sends same byte back //
/////////////////////////////////////////////////////


// LIBRARIES
#include "Arduino.h"
#include <Wire.h>
#define SLAVE_ADDRESS 0x04

byte dim = 128;


void receiveData(int byteCount){
  while(Wire.available()) {
    dim = Wire.read();
    Serial.println(dim);
  }
}

// callback for sending data
void sendData(){
  Wire.write(dim);
}

void setup() {
  Serial.begin(9600); // start serial for output
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  Serial.println("Ready!");
}

void loop() {

}
