// -*- coding: utf-8 -*-
// © 2021 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// LIBRARIES
#include "Arduino.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

#define UNIT_ADDRESS 0x04
#define BNO055_SAMPLERATE_DELAY_MS (100)

Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);
byte dim = 128;
byte euler_x = 0;

void receiveData(int byteCount){
  while(Wire.available()) {
    dim = Wire.read();
    Serial.print("Read: ");
    Serial.println(dim);
  }
}

// callback for sending data
void sendData(){
  Wire.write(euler_x);
}

void setup() {
  Serial.begin(115200); // start serial for output

  /* Initialise the sensor */
  if(!bno.begin()) {
    /* There was a problem detecting the BNO055 ... check connections */
    Serial.print("No BNO055 detected. Check wiring or I2C address");
    while(1);
  }

  Wire.begin(UNIT_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  Serial.println("Ready!");
}

void loop() {

  // Possible vector values can be:
  // - VECTOR_ACCELEROMETER - m/s^2
  // - VECTOR_MAGNETOMETER  - uT
  // - VECTOR_GYROSCOPE     - rad/s
  // - VECTOR_EULER         - degrees
  // - VECTOR_LINEARACCEL   - m/s^2
  // - VECTOR_GRAVITY       - m/s^2
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  euler_x = euler.x();
  //Serial.println(euler_x);

  delay(BNO055_SAMPLERATE_DELAY_MS);

}
