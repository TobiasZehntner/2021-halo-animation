import logging


class Base:

    id = False
    name = False

    def __init__(self, values=None):
        self.logger = logging.getLogger("base")

        for key, value in values.items():
            self[key] = value

    def __setitem__(self, key, value):
        if key >= len(self):
            self.vector.extend(key + 1)
        self.vector[key] = value

    def run(self):
        pass

    def update(self):
        pass
