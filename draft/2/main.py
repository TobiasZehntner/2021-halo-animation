# from . import models

# -c -> Run as controller
# create controller instance with CONTROLLER_CONFIG

# -u <id> -> Run as unit
# create unit instance with UNIT_CONFIG

# from config_data import CONFIG, CONTROLLER_VALS
import argparse
import logging
import sys

# from models import controller
# logging.basicConfig(level=logging.INFO, format=CONFIG["logging"]["format"])
halo_logger = logging.getLogger("halo")


class ArgParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write("error: %s\n" % message)
        self.print_usage(sys.stderr)
        sys.exit(2)


parser = ArgParser(description="Run Halo controller")
parser.add_argument("--debug", help="Run in Debug Mode", action="store_true")
parser.add_argument("-hl", "--headless", help="Run without User Interface")
# group = parser.add_mutually_exclusive_group()
# group.add_argument("-c", "--controller", help="Run as controller",
# action="store_true")
# group.add_argument("-u", "--unit", type=int, choices=[0, 1, 2],
# help="Run as unit <id>")

args = parser.parse_args()

# if not len(sys.argv) > 1:
#     parser.print_help()
#     sys.exit(2)
if args.headless:
    halo_logger.info("Running Halo controller without user interface")
if args.debug:
    halo_logger.info("Debug Mode active")

if __name__ == "__main__":
    # controller = controller.HaloController(CONTROLLER_VALS)
    # controller.run()
    pass
