# © 2020 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

########################################################################################
# HALO - Config Values
# Constants
########################################################################################

VALUES = {
    "fps": 30,
    "is_debug": False,
    "is_draw": True,
    "defaults": {
        "rpm": 10,  # TODO calculate default rpm depending on height
        "radius": 2.0,
    },
    "setup": {
        "num_units": 8,
        "distance": {"x": 4.5, "y": 6.5},  # In meters between units
        "matrix_pos": {  # row, column
            1: (0, 0),
            2: (0, 1),
            3: (1, 0),
            4: (1, 1),
            5: (2, 0),
            6: (2, 1),
            7: (3, 0),
            8: (3, 1),
        },
        "height_diff_ratio": {  # Max diff between neighbors
            1: 1,
            2: 5,
            3: 6,
            4: 2,
            5: 3,
            6: 7,
            7: 8,
            8: 4,
        },
    },
    "network": {"ip": {}, "wifi": {}},
    "draw": {"window": {"width": 700, "height": 1000}, "m_to_px_ratio": 50},
}
