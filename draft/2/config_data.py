# © 2020 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

########################################################################################
# HALO - Config Data
# Data prepared for import and use
########################################################################################

from config import VALUES


def m_to_px(m):
    ratio = VALUES["draw"]["m_to_px_ratio"]
    return m * ratio


matrix_pos = VALUES["setup"]["matrix_pos"]
distance = VALUES["setup"]["distance"]
unit_pos_m = {}
for unit_id, pos in matrix_pos.items():
    unit_pos_m[unit_id] = (pos[0] * distance["x"], pos[1] * distance["y"])
unit_pos_px = {}
for unit_id, pos in unit_pos_m.items():
    unit_pos_px[unit_id] = (m_to_px(pos[0]), m_to_px(pos[1]))


unit_config = {}
for unit_id in range(1, VALUES["setup"]["num_units"]):
    unit_config[unit_id] = {
        "pos": {"x": unit_pos_px[unit_id][0], "y": unit_pos_px[unit_id][1]}
    }

# Center drawing in screen
draw_config = VALUES["draw"]
dist_x = m_to_px(VALUES["setup"]["distance"]["x"])
dist_y = m_to_px(VALUES["setup"]["distance"]["y"])
draw_pos_x = (draw_config["window"]["width"] / 2) - (dist_x / 2)
draw_pos_y = (draw_config["window"]["height"] / 2) - ((dist_y * 3) / 2)

draw_config.update({"pos": {"x": draw_pos_x, "y": draw_pos_y}})

defaults = VALUES["defaults"]
defaults["radius"] = m_to_px(defaults["radius"])

CONTROLLER_VALS = {
    "dt": 1 / VALUES["fps"],
    "is_debug": VALUES["is_debug"],
    "is_draw": VALUES["is_draw"],
    "unit_config": unit_config,
    "defaults": defaults,
    "draw": draw_config,
    "max_distance": dist_x * 2,  # Max distance between two bulbs (zero dim)
}
UNIT_VALS = {}

CONFIG = {"logging": {"format": "%(asctime)s - %(levelname)s - %(message)s"}}
