# © 2020 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

########################################################################################
# HALO - Light Unit - Main Loop
# Runs main loop with constants from config.py
# - Receive brightness and motor speed from Controller
# - Send brightness to Dimmer Controller (Arduino)
# - Send speed to Motor
# - Read Accelerometer
# - Send Angle to Controller
########################################################################################

import logging
import time

# Allow import from outside module
d = dirname(dirname(abspath(__file__)))  # noqa isort:skip
sys.path.insert(0, d)  # noqa isort:skip
from config import CONSTANTS, DEBUG, DRAW  # noqa isort:skip

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def init():
    pass


def update(dt):
    pass


def main():
    """
    Running the main loop
    """
    try:
        dt = CONSTANTS.get("DT")
        counter = 0
        start_time = time.time()
        while True:

            update(dt)

            # Aim for precise interval
            counter += 1
            sleep = start_time + (dt * counter) - time.time()
            if sleep > 0:
                time.sleep(sleep)
            else:
                logging.warning("Too slow: loop took longer than DT. Reduce f/s.")

    except KeyboardInterrupt:
        logging.info("\nExiting...")
    except Exception as e:
        logging.info("Error: %s" % e)
    finally:
        logging.info("\nShutting down...")
        # Turn off any connections/dependants
        logging.info("Done.")


if __name__ == "__main__":
    init()
    main()
