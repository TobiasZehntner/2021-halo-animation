# © 2020 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

########################################################################################
# HALO - Controller - Main Loop
# Runs main loop with constants from config.py
# - Init grid of units
# - Draw 2D controller
# - Calculate distances
# - Update units
########################################################################################


import logging
import math
import random
import sys
from os.path import abspath, dirname

import light_unit
import pyglet
from pyglet import gl
from scipy.constants import g as GRAVITY

# Allow import from outside module
d = dirname(dirname(abspath(__file__)))  # noqa isort:skip
sys.path.insert(0, d)  # noqa isort:skip
from config_data import CONFIG  # noqa isort:skip

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

DRAW = CONFIG["is_draw"]
DEBUG = CONFIG["is_debug"]

window = pyglet.window.Window(
    CONFIG["draw"]["window"]["width"], CONFIG["draw"]["window"]["height"]
)


units = []
matrix = []

groups = {
    "back_ground": pyglet.graphics.OrderedGroup(0),
    "middle_ground": pyglet.graphics.OrderedGroup(1),
    "fore_ground": pyglet.graphics.OrderedGroup(2),
}


HEIGHT = 14  # Triangle height in meters
HEIGHT_DIFF = 1  # Max diff in install height between units


def get_speed(height_m):
    """
    @param height_m: height of conical pendulum triangle
    @return: rpm
    """
    return ((math.sqrt(GRAVITY / height_m)) / (2 * math.pi)) * 60


def init():
    """
    Set up grid of light units.
    """

    id_sequence = 1
    min_rpm = get_speed(HEIGHT)
    max_rpm = get_speed(HEIGHT - HEIGHT_DIFF)
    # rpm_increase = (max_rpm - min_rpm) / NUM_UNITS
    # rpm = min_rpm
    # direction = 1
    WINDOW_H = False
    WINDOW_W = False
    ROWS = False
    COLUMNS = False
    SPACING = False
    y = (WINDOW_H - ((ROWS - 1) * SPACING)) / 2
    start_rotation = 0

    defaults = CONFIG["defaults"]
    for i in range(0, ROWS):
        matrix.append([])
        x = (WINDOW_W - ((COLUMNS - 1) * SPACING * 1.33)) / 2
        for j in range(0, COLUMNS):
            matrix[i].append(j)
            # rpm = randrange(20, 40) / 7  # Sync up after 7mins
            rpm = random.uniform(min_rpm, max_rpm)
            # Each column turning same direction
            # if j % 2 == 0:
            #     rpm *= -1

            # Each row turning same direction
            # if j % 2 == 0:
            #     direction *= -1
            # rpm *= direction

            unit = light_unit.LightUnit(
                groups,
                x=x,
                y=y,
                matrix_row=i,
                matrix_col=j,
                radius=defaults["radius"],
                rpm=rpm,
                start_rotation=start_rotation,
            )
            unit.id = id_sequence
            units.append(unit)
            matrix[i][j] = unit.endpoint
            x += SPACING * 1.33
            id_sequence += 1
            start_rotation += 120
            # rpm += rpm_increase
        y += SPACING  # * 1.33  # Vert spacing bigger in church

        for unit in units:
            unit.matrix_neighbor_pos = get_neighbor_positions(
                matrix, unit.matrix_pos[0], unit.matrix_pos[1]
            )


def get_neighbor_positions(matrix, row_num, col_num):
    neighbor_positions = []
    for i, row in enumerate(matrix):
        if row_num - 1 <= i <= row_num + 1:
            for j, _col in enumerate(row):
                if col_num - 1 <= j <= col_num + 1:
                    if i == row_num and j == col_num:
                        continue
                    else:
                        neighbor_positions.append((i, j))
    return neighbor_positions


def get_distance(x1y1, x2y2):
    a = abs(x1y1[0] - x2y2[0])
    b = abs(x1y1[1] - x2y2[1])
    return math.sqrt((a ** 2) + (b ** 2))


def update(dt):

    if DRAW:
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)
        gl.glLoadIdentity()
        # Center drawing to window
        gl.glPushMatrix()
        gl.glTranslatef(CONFIG["draw"]["pos"]["x"], CONFIG["draw"]["pos"]["y"], 0.0)

    for unit in units:
        # Update rotation
        unit.update_rotation(dt)
        matrix[unit.matrix_pos[0]][unit.matrix_pos[1]] = unit.endpoint

    for unit in units:
        # Update brightness
        distances = {}
        for neighbor in unit.matrix_neighbor_pos:
            distances[neighbor] = get_distance(
                unit.endpoint, matrix[neighbor[0]][neighbor[1]]
            )

        min_distance = min(distances.values())
        unit.update_brightness(min_distance, CONFIG["max_distance"])

        if DRAW:
            # Draw Unit
            neighbor = False
            if DEBUG:
                # Draw line to measured unit
                closest_pos = False
                for pos, dist in distances.items():
                    if dist == min_distance:
                        closest_pos = pos
                neighbor = matrix[closest_pos[0]][closest_pos[1]]
            unit.draw(neighbor)

    if DRAW:
        gl.glPopMatrix()


def main():
    """
    Set up main loop
    """
    try:
        dt = CONFIG["dt"]
        pyglet.clock.schedule_interval(update, dt)

        pyglet.app.run()

    except KeyboardInterrupt:
        logging.info("\nExiting...")
    except Exception as e:
        logging.info("Error: %s" % e)
    finally:
        logging.info("Shutting down...")
        # Turn off any connections/dependants
        logging.info("Done.")


if __name__ == "__main__":
    init()
    main()
