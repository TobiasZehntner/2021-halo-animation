# © 2020 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

########################################################################################
# HALO - Controller - LightUnit Class
# Contains all variables and functions for one unit
# - Connects to its RPi Unit
# - Receive angle and motor position from Unit
# - Send brightness and speed to Unit
# - 2D drawing of unit
########################################################################################

import math

import pyglet
from pyglet import gl


def _get_circle_vertices(x, y, radius, fill=True):
    pts = 360
    s = math.sin(2 * math.pi / pts)
    c = math.cos(2 * math.pi / pts)

    vertices = []
    if fill:
        vertices += [x, y]
    dx, dy = radius, 0
    for _i in range(pts + 1):
        vertices += [x + dx, y + dy]
        dx, dy = (dx * c - dy * s), (dy * c + dx * s)

    return vertices


class LightUnit:
    def __init__(
        self,
        groups,
        x=0,
        y=0,
        matrix_row=0,
        matrix_col=0,
        radius=50,
        rpm=5,
        start_rotation=0,
    ):
        self.id = False

        self.x = x
        self.y = y

        self.radius = radius

        self.rpm = rpm
        self.rotation = start_rotation
        self.endpoint = (x, y + radius)

        self.matrix_pos = (matrix_row, matrix_col)
        self.matrix_neighbor_pos = []

        self.brightness = 50

        self.batch = pyglet.graphics.Batch()

        bulb_radius, bulb_light_radius, throw_light_radius = self._get_radius()

        circle_x, circle_y = radius, 0
        self.circle_x, self.circle_y = circle_x, circle_y

        # Draw cable and bulb static
        self.cable = self.batch.add(
            2,
            pyglet.gl.GL_LINES,
            groups.get("fore_ground"),
            ("v2f/static", (0, 0, circle_x, circle_y)),
            ("c4B/static", (0, 0, 0, 255) * 2),
        )
        # Draw bulb as hollow circle
        bulb_vertices = _get_circle_vertices(
            circle_x, circle_y, bulb_radius, fill=False
        )
        hollow_circle_index = len(bulb_vertices) // 2
        self.bulb = self.batch.add(
            hollow_circle_index,
            gl.GL_LINE_LOOP,
            groups.get("middle_ground"),
            ("v2f/static", bulb_vertices),
            ("c4B/static", (120, 120, 120, 76) * hollow_circle_index),
        )
        # Draw light as filled circle with modifiable vertices
        bulb_light_vertices = _get_circle_vertices(
            circle_x, circle_y, bulb_light_radius
        )
        # Add midpoint to fill circle
        filled_circle_index = hollow_circle_index + 1
        # Inside color - outside color gradient
        bulb_light_colors = (249, 242, 204, 255) + (
            (249, 242, 204, 0) * hollow_circle_index
        )
        self.bulb_light = self.batch.add(
            filled_circle_index,
            gl.GL_TRIANGLE_FAN,
            groups.get("middle_ground"),
            ("v2f/stream", bulb_light_vertices),
            ("c4B/static", bulb_light_colors),
        )
        throw_light_vertices = _get_circle_vertices(
            circle_x, circle_y, throw_light_radius
        )
        throw_light_colors = (213, 148, 67, 80) + ((0, 0, 0, 0) * hollow_circle_index)

        self.throw_light = self.batch.add(
            filled_circle_index,
            gl.GL_TRIANGLE_FAN,
            groups.get("back_ground"),
            ("v2f/stream", throw_light_vertices),
            ("c4B/static", throw_light_colors),
        )

    def _get_radius(self):
        bulb_radius = self.radius / 40
        bulb_light_radius = bulb_radius * 2 / 100 * self.brightness
        throw_light_radius = bulb_light_radius * 50
        return bulb_radius, bulb_light_radius, throw_light_radius

    def update_rotation(self, dt):
        """
        deg/s = rpm * 6
        deg = deg/s * dt
        """
        self.rotation += self.rpm * 6 * dt
        self.endpoint = self.get_endpoint(self.rotation)

    def update_brightness(self, distance, max_distance):
        self.brightness = 100 - min(100, (100 / max_distance * distance))
        bulb_radius, bulb_light_radius, throw_light_radius = self._get_radius()

        self.bulb_light.vertices = _get_circle_vertices(
            self.circle_x, self.circle_y, bulb_light_radius
        )
        self.throw_light.vertices = _get_circle_vertices(
            self.circle_x, self.circle_y, throw_light_radius
        )

    def get_endpoint(self, angle):
        angle_radians = -math.radians(angle)
        return (
            math.cos(angle_radians) * self.radius + self.x,
            math.sin(angle_radians) * self.radius + self.y,
        )

    def draw(self, neighbor):
        if neighbor:
            # draw measured distance to neighbor
            gl.glBegin(gl.GL_LINES)
            gl.glColor4f(1.0, 0.0, 0.0, 1)
            gl.glVertex2f(self.endpoint[0], self.endpoint[1])
            gl.glVertex2f(neighbor[0], neighbor[1])
            gl.glEnd()

        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        gl.glPushMatrix()
        gl.glTranslatef(self.x, self.y, 0.0)
        gl.glRotatef(-self.rotation, 0, 0, 1)

        self.batch.draw()

        gl.glTranslatef(-self.x, -self.y, 0.0)
        gl.glPopMatrix()
