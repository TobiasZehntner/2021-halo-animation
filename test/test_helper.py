import unittest

from halo.helpers import helper


class TestBase(unittest.TestCase):
    def test_ping(self):
        """
        Test that pinging a server works. Fails if no connection.
        """
        self.assertEqual(
            helper.ping("google.com"), True, "Ping failed. Check network connection."
        )


if __name__ == "__main__":
    unittest.main()
