import logging

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

TEST_SETUP = {
    "fps": 50,
    "hostname": "halo-unit-1",
    "arduino_bus": 1,
    "arduino_address": 0x04,
    "dimmer": {"min": 105, "max": 40},
    "imu_bus": 1,
    "imu_address": 0x28,
}
