import logging
import sys

import pigpio
from manual_test_setup import TEST_SETUP

# ARDUINO: upload i2c_test sketch

# Connect to local pi
pi = pigpio.pi(f"{TEST_SETUP['hostname']}.local")

if not pi.connected:
    logging.error("Could not connect to pi")
    sys.exit(2)
logging.info(f"Pi Connected: {pi.connected}")

arduino = pi.i2c_open(TEST_SETUP["arduino_bus"], TEST_SETUP["arduino_address"])

if arduino >= 0:
    logging.info("Connected to Arduino")

byte = 10
logging.info(f"Sending byte: {byte}")
pi.i2c_write_byte(arduino, byte)
result = pi.i2c_read_byte(arduino)
logging.info(f"Reading byte: {result}")
assert result == byte
logging.info("Test successful")

pi.i2c_close(arduino)
pi.stop()
