import logging
import sys

import pigpio
from manual_test_setup import TEST_SETUP

# Connect to local pi
pi = pigpio.pi(f"{TEST_SETUP['hostname']}.local")

if not pi.connected:
    logging.error("Could not connect to pi")
    sys.exit(2)
logging.info(f"Connected: {pi.connected}")

logging.info("Writing to PIN 4: HIGH")
pi.write(4, 1)
logging.info(f"Reading PIN 4: {pi.read(4)}")
assert pi.read(4) == 1

logging.info("Writing to PIN 4: LOW")
pi.write(4, 0)
logging.info(f"Reading PIN 4: {pi.read(4)}")
assert pi.read(4) == 0

logging.info("Test successful")
