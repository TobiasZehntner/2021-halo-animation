import unittest

from halo.controller import base


class TestBase(unittest.TestCase):
    def test_base_init(self):
        """
        Test that Base class can be initiated with values
        """
        # TODO check if you can have required attribute (or create check in class)
        vals = {"id": 4, "name": "test base"}
        base_object = base.Base(**vals)
        self.assertEqual(
            base_object.id, vals["id"], "Value 'id' not correctly assigned"
        )
        self.assertEqual(
            base_object.name, vals["name"], "Value 'name' not correctly assigned"
        )
        non_existing_vals = {"non_existing_field": 36}
        self.assertRaises(AttributeError, lambda: base.Base(**non_existing_vals))

    def test_base_inherit(self):
        """
        Test that inherited classes can assign own and inherited attributes
        """
        vals = {"id": 4, "name": "test base", "new_field": "new value"}

        class BaseInherit(base.Base):
            __slots__ = ["new_field"]

        base_inherit_object = BaseInherit(**vals)

        self.assertEqual(
            base_inherit_object.id,
            vals["id"],
            "Inherited value 'id' not correctly assigned",
        )
        self.assertEqual(
            base_inherit_object.name,
            vals["name"],
            "Inherited value 'name' not correctly assigned",
        )
        self.assertEqual(
            base_inherit_object.new_field,
            vals["new_field"],
            "Value 'new_field' not correctly assigned",
        )


if __name__ == "__main__":
    unittest.main()
